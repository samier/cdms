-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2013 at 09:13 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cdms`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `projectId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `commentDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `comment`, `projectId`, `userId`, `commentDate`) VALUES
(1, 'local admin comment', 10, 1, '2013-09-13 07:54:14'),
(2, 'testing demo admin ', 1, 1, '2013-09-13 09:01:16'),
(3, 'testing demo admin ', 1, 1, '2013-09-13 09:01:28'),
(4, 'this testing demo', 2, 1, '2013-09-13 09:02:48'),
(5, 'testing second testing second testing second testing second testing second testing second ', 2, 1, '2013-09-13 09:10:01'),
(6, ' new comment', 2, 1, '2013-09-13 10:00:21'),
(7, ' latest comment testing', 2, 1, '2013-09-13 10:36:31'),
(8, ' dfrtgdr vhfvgjuhft', 1, 1, '2013-09-13 11:22:46'),
(9, ' testing date testing date', 11, 1, '2013-09-13 12:28:20'),
(10, '  Comment\r\n Comment', 3, 1, '2013-09-13 12:33:33'),
(11, '  Comment latest', 11, 1, '2013-09-13 12:34:22'),
(12, ' testing datetesting datetesting date', 12, 1, '2013-09-13 13:54:24'),
(13, '  Comment\r\n Comment\r\n', 12, 1, '2013-09-13 13:54:52'),
(14, ' testing date assign datetesting date assign date', 13, 1, '2013-09-14 05:00:12'),
(15, ' demo demo demo demo demo demo ', 14, 1, '2013-09-14 05:01:20'),
(16, ' demo new', 14, 1, '2013-09-14 05:01:57'),
(17, ' eeeeeee', 15, 1, '2013-09-14 05:13:03'),
(18, ' date assign date', 13, 1, '2013-09-14 10:14:18'),
(19, ' Test on 16th date sep', 16, 1, '2013-09-16 04:59:05'),
(20, ' This is just testing', 17, 1, '2013-09-16 06:18:06'),
(21, ' This project has been started by wama', 17, 1, '2013-09-16 06:18:46'),
(22, 'assign to samier', 18, 1, '2013-09-16 06:24:30'),
(23, ' cache cache', 19, 1, '2013-09-16 06:40:43');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `authorized` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `uri`, `method`, `params`, `api_key`, `ip_address`, `time`, `authorized`) VALUES
(1, 'api/example/user/id/2', 'get', 'a:1:{s:2:"id";s:1:"2";}', '', '127.0.0.1', 1379145691, 1),
(2, 'api/example/users', 'get', NULL, '', '127.0.0.1', 1379145740, 1),
(3, 'api/example/users', 'get', NULL, '', '127.0.0.1', 1379145861, 1),
(4, 'api/example/users', 'get', NULL, '', '127.0.0.1', 1379146096, 1),
(5, 'api/example/users/format', 'get', 'a:1:{s:6:"format";b:0;}', '', '127.0.0.1', 1379146127, 1),
(6, 'api/example/users/format', 'get', 'a:1:{s:6:"format";b:0;}', '', '127.0.0.1', 1379146134, 1),
(7, 'api/example/users/formate', 'get', 'a:1:{s:7:"formate";b:0;}', '', '127.0.0.1', 1379146140, 1),
(8, 'api/example/users/formate/json', 'get', 'a:1:{s:7:"formate";s:4:"json";}', '', '127.0.0.1', 1379146146, 1),
(9, 'api/example/users/formate/json', 'get', 'a:1:{s:7:"formate";s:4:"json";}', '', '127.0.0.1', 1379146149, 1),
(10, 'api/example/users/format/json', 'get', 'a:1:{s:6:"format";s:4:"json";}', '', '127.0.0.1', 1379148841, 1),
(11, 'api/example/projects', 'get', NULL, '', '127.0.0.1', 1379149331, 1),
(12, 'api/projectapi/projects', 'get', NULL, '', '127.0.0.1', 1379149507, 1),
(13, 'api/projectapi/projects/format/json', 'get', 'a:1:{s:6:"format";s:4:"json";}', '', '127.0.0.1', 1379149627, 1),
(14, 'api/projectapi/projects/format/json', 'get', 'a:1:{s:6:"format";s:4:"json";}', '', '127.0.0.1', 1379149765, 1),
(15, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379149770, 1),
(16, 'api/projectapi/projects/format', 'get', 'a:1:{s:6:"format";b:0;}', '', '127.0.0.1', 1379149774, 1),
(17, 'api/projectapi/projects/format', 'get', 'a:1:{s:6:"format";b:0;}', '', '127.0.0.1', 1379149777, 1),
(18, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379149781, 1),
(19, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379149960, 1),
(20, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379149964, 1),
(21, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '192.168.1.102', 1379150406, 1),
(22, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379150465, 1),
(23, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379150529, 1),
(24, 'api/projectapi/projects/format/json', 'get', 'a:1:{s:6:"format";s:4:"json";}', '', '127.0.0.1', 1379155391, 1),
(25, 'api/projectapi/projects', 'get', NULL, '', '127.0.0.1', 1379163121, 1),
(26, 'api/projectapi/projects', 'get', NULL, '', '127.0.0.1', 1379163528, 1),
(27, 'api/projectapi/projects', 'get', NULL, '', '127.0.0.1', 1379163627, 1),
(28, 'api/projectapi/projects/format/json', 'get', 'a:1:{s:6:"format";s:4:"json";}', '', '127.0.0.1', 1379163655, 1),
(29, 'api/projectapi/projects', 'get', NULL, '', '127.0.0.1', 1379163788, 1),
(30, 'api/projectapi/projects', 'get', NULL, '', '127.0.0.1', 1379163905, 1),
(31, 'api/projectapi/projects', 'get', NULL, '', '127.0.0.1', 1379166822, 1),
(32, 'api/projectapi/projects/format/csv', 'get', 'a:1:{s:6:"format";s:3:"csv";}', '', '127.0.0.1', 1379167202, 1),
(33, 'api/projectapi/projects', 'get', NULL, '', '127.0.0.1', 1379307099, 1),
(34, 'api/projectapi/projects', 'get', NULL, '', '127.0.0.1', 1379307564, 1),
(35, 'api/projectapi/projects', 'get', NULL, '', '127.0.0.1', 1379308091, 1),
(36, 'api/projectapi/projects/format/json', 'get', 'a:1:{s:6:"format";s:4:"json";}', '', '127.0.0.1', 1379308105, 1),
(37, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379308145, 1),
(38, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379308385, 1),
(39, 'api/projectapi/projects/format/csv', 'get', 'a:1:{s:6:"format";s:3:"csv";}', '', '127.0.0.1', 1379310341, 1),
(40, 'api/projectapi/projects/format/csv', 'get', 'a:1:{s:6:"format";s:3:"csv";}', '', '127.0.0.1', 1379310401, 1),
(41, 'api/projectapi/projects/format/csv', 'get', 'a:1:{s:6:"format";s:3:"csv";}', '', '127.0.0.1', 1379310422, 1),
(42, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379312336, 1),
(43, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379312339, 1),
(44, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379312893, 1),
(45, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379312911, 1),
(46, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379313499, 1),
(47, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379313847, 1),
(48, 'api/projectapi/projects/format/xml', 'get', 'a:1:{s:6:"format";s:3:"xml";}', '', '127.0.0.1', 1379313873, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` mediumtext CHARACTER SET utf8 NOT NULL,
  `status` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT 'n' COMMENT 'n=new,o=On-Progress,p=Pending,c=Closed',
  `tags` mediumtext CHARACTER SET utf8 NOT NULL,
  `keywords` mediumtext CHARACTER SET utf8 NOT NULL,
  `deadlineDate` date NOT NULL,
  `assignedDate` date NOT NULL,
  `editedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dateCreated` date NOT NULL,
  `assignedPerson` int(11) NOT NULL,
  `createdPerson` int(11) NOT NULL,
  `upldatedPerson` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `projectName`, `description`, `status`, `tags`, `keywords`, `deadlineDate`, `assignedDate`, `editedDate`, `dateCreated`, `assignedPerson`, `createdPerson`, `upldatedPerson`) VALUES
(17, 'Content Data Management System', 'this project maintaints project data ', 'n', 'employee', 'project,task', '2013-09-19', '2013-09-16', '2013-09-16 06:18:46', '2013-09-16', 3, 1, 1),
(18, 'UTM - Firewall', 'security,bandwidth management,natting', 'n', 'security', 'content filtering', '2013-09-27', '2013-09-16', '2013-09-16 06:24:30', '2013-09-16', 2, 1, 0),
(19, 'Process and Cache - Memory management ', 'for performace level', 'o', 'cache', 'process', '2013-09-20', '2013-09-16', '2013-09-16 06:40:43', '2013-09-16', 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login_info`
--

CREATE TABLE IF NOT EXISTS `tbl_login_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `type` varchar(1) NOT NULL COMMENT 'a=admin,c=Content Producers',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_login_info`
--

INSERT INTO `tbl_login_info` (`id`, `username`, `password`, `type`, `created`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'a', '2013-09-11 18:30:00'),
(2, 'samier', '21232f297a57a5a743894a0e4a801fc3', 'c', '2013-09-13 03:01:22'),
(3, 'wama', '21232f297a57a5a743894a0e4a801fc3', 'c', '2013-09-13 03:01:22');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
