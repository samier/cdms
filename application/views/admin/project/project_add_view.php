<?php 

  if(isset($id)) {
    $form_url = '/project/add/' . $id;
  } else {
    $form_url = '/project/add/';
  }
	
  if(validation_errors() || isset($unique_project_name)) { 
?>
  <div class="alert alert-success">
	 <?php 
	 	echo validation_errors('<div>',',</div>'); 
	    if(isset($unique_project_name)) 
			echo $unique_project_name;
	 ?>
	</div>
<?php } ?>	
<form class="form-horizontal" action="<?php echo $form_url;?>" method="post">
  <fieldset>
    <legend>Add/Edit Project</legend>
		
    <div class="control-group <?php if(form_error('projectName')) { ?>error<?php } ?>">
      <label class="control-label" for="input01">Project Name</label>
      <div class="controls">
        <input type="text" class="input-xlarge" <?php echo isset($user_type) && $user_type == 'c' ? 'disabled="disabled"' : '' ?>  name="projectName" id="name"  value="<?php echo set_value('projectName',isset($edit_result['projectName']) ? $edit_result['projectName']:'');?>" / >
      </div>
    </div>
	<div class="control-group <?php if(form_error('description')) { ?>error<?php } ?>">
      <label class="control-label" for="input01">Description</label>
      <div class="controls">
        <textarea name="description" id="description" <?php echo isset($user_type) && $user_type == 'c' ? 'disabled="disabled"' : '' ?> ><?php echo set_value('description',isset($edit_result['description']) ? $edit_result['description']:'');?></textarea>
      </div>
    </div>	

    <div class="control-group <?php if(form_error('deadlineDate')){ ?>error<?php } ?>">
      <label class="control-label" for="input01">Deadline Date</label>
      <div class="controls">
        <input type="text" class="input-xlarge" name="deadlineDate" id="deadlineDate"  value="<?php echo set_value('deadlineDate',isset($edit_result['deadlineDate']) ? $edit_result['deadlineDate']:'');?>" <?php echo isset($user_type) && $user_type == 'c' ? 'disabled="disabled"' : '' ?> / >
      </div>
    </div>
    <div class="control-group <?php if(form_error('status')){ ?>error<?php } ?>">
      <label class="control-label" for="input01">Status</label>
      <div class="controls">
	  	<?php 
			$options = array(
                  'n'  => 'New',
                  'o'  => 'On-Progress',
                  'p'  => 'Pending',
                  'c'  => 'Closed',
                );
			echo form_dropdown('status', $options,set_value('status',isset($edit_result['status']) ? $edit_result['status']:'') );	
		?>		
      </div>
    </div>
	 
   <div class="control-group <?php if(form_error('tags')) { ?>error<?php } ?>">
      <label class="control-label" for="input01" >tags</label>
      <div class="controls">
        <textarea name="tags" id="tags" <?php echo isset($user_type) && $user_type == 'c' ? 'disabled="disabled"' : '' ?>><?php echo set_value('tags',isset($edit_result['tags']) ? $edit_result['tags']:'');?></textarea>
      </div>
    </div>
	
	<div class="control-group <?php if(form_error('keywords')) { ?>error<?php } ?>">
      <label class="control-label" for="input01">keywords</label>
      <div class="controls">
        <textarea name="keywords" id="keywords" <?php echo isset($user_type) && $user_type == 'c' ? 'disabled="disabled"' : '' ?>><?php echo set_value('keywords',isset($edit_result['keywords']) ? $edit_result['keywords'] : '');?></textarea>
      </div>
    </div>
	
	<div class="control-group <?php if(form_error('assignedPerson')){ ?>error<?php } ?>">
      <label class="control-label" for="input01">Assigned To</label>
      <div class="controls">
	  	<?php 
			$disabled = isset($user_type) && $user_type == 'c' ? 'disabled="disabled"' : '';
			echo form_dropdown('assignedPerson', $content_Producer_options,set_value('assignedPerson',isset($edit_result['assignedPerson']) ? $edit_result['assignedPerson']:''),$disabled);	
		?>		
      </div>
    </div>	
    <div class="control-group" style="margin-bottom:5px; font-size:16px;"> <div class="controls">Comments</div></div>
 <?php   
    if (isset($comments)) :
		foreach ($comments as $comment): ?>	
     <div class="control-group <?php if(form_error('comment')) { ?>error<?php } ?>" style="margin-bottom:5px;">
      <label class="control-label" for="input01"> <?php echo isset($comment['username']) ? $comment['username'] : 'Comment '; ?> </label>
      <div class="controls">
          <label class="control-label" style="text-align: left; width: 230px;"> <?php echo set_value('comment',isset($comment['comment']) ? $comment['comment']:'');?></label>
      </div>
    </div>
 <?php
		endforeach; 
	endif  ?>    
    
    <div class="control-group <?php if(form_error('comment')) { ?>error<?php } ?>">
      <label class="control-label" for="input01">Comment</label>
      <div class="controls">
        <textarea name="comment" id="comment"> </textarea>
      </div>
    </div> 
    <div class="form-actions">
      <button type="submit" class="btn btn-primary">Save changes</button>
      <button type="button" class="btn" onclick="window.location.href='/project'">Cancel</button>
    </div>
  </fieldset>
</form>


<h1>Project Details</h1>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="project_table">
	<thead>
		<tr>
			<th>Id</th>			
			<th>Status</th>
			<th>Tags</th>
			<th>Keywords</th>
			<th>Created By</th>
			<th>Updated By</th>			
			<th>Assigned To</th>
			<th>Date Assigned</th>	
			<th>Deadline Date</th>
			<th>Edited Date</th>
			<th>Created Date</th>
            <th>Latest Comment</th>					
		</tr>
	</thead>
	<tbody>	
	</tbody>	
</table>