<h1>Project Listing</h1>
<div class="pull-right">
<?php if ($user_type == 'a'): ?>
<button type="button" class="btn btn-info" onclick="window.location.href='/project/add/'">Add New Project</button>
<?php endif; ?>
</div>
<br />
<br />
<table cellpadding="0" cellspacing="0" border="0" class="display" id="project_table">
	<thead>
		<tr>
			<th>Id</th>
			<th>Project Name</th>
			<th>description</th>
			<th>Status</th>
			<th>Tags</th>
			<th>Keywords</th>
			<th>Created By</th>
			<th>Updated By</th>			
			<th>Assigned To</th>
			<th>Date Assigned</th>	
			<th>Deadline Date</th>
			<th>Edited Date</th>
			<th>Created Date</th>
            <th>Latest Comment</th>					
		</tr>
	</thead>
	<tbody>	
	</tbody>	
</table>