<div class="container">
<?php
  if(validation_errors()) { 
?>
  <div class="alert alert-success">
	 <?php 
	 	echo validation_errors('<div>','</div>'); 
	 ?>
	</div>
<?php } ?>	

  <form name="frmlogin" id="frmlogin" action="/login/submit" method="post">
    <table width="200" border="0" cellspacing="0" cellpadding="0" summary="Sign in form">
      <tr>
        <td colspan="2">
          <h3>Login</h3>
        </td>
      </tr>      
      <?php if($this->session->flashdata('err_msg')):?>
        <tr>
          <td>&nbsp;</td>
          <td>
            <span class="label important">
              <?php echo form_error('username');?>
              <?php echo form_error('password');?>
              <?php echo $this->session->flashdata('err_msg');?>
            </span>
          </td>
        </tr>
        <?php endif;?>
          <tr>
            <td align="left">
              <span class="label important">Username</span>
            </td>
            <td align="left" height="24">
              <input class="xlarge" id="username" name="username" size="30" type="text">
            </td>
          </tr>
          <tr>
            <td align="left">
              <span class="label important">Password</span>
            </td>
            <td align="left">
              <input class="xlarge" id="password" name="password" size="30" type="password">
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <div class="actions" align="left">
                <button type="submit" class="btn btn-primary">Submit</button>&nbsp;&nbsp;&nbsp;
                <button type="reset" class="btn">Cancel</button>
              </div>
            </td>
          </tr>
          <tr>
            <td height="5"></td>
            <td></td>
          </tr>
    </table>
     <br />
    <div style="font-size:16px;">
        <p>
            Admin Access : admin/admin
        </p>
        <p>
            Content Producers Access : samier/admin
        </p>
        <p>
            Content Producers Access : wama/admin
        </p>
        <br />
        <p>
            API URL :
            
            <div style="padding-left:10px;"> xml : http://cdms.localhost/api/projectapi/projects/format/xml </div>
            <div style="padding-left:10px;"> json : http://cdms.localhost/api/projectapi/projects/format/json </div>
            <div style="padding-left:10px;"> csv: http://cdms.localhost/api/projectapi/projects/format/csv </div>
            
        </p>
    </div>
  </form>
</div>