<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="en" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <meta name="" content="" />
    <link rel="stylesheet" href="/public/css/bootstrap.css">
    <link rel="stylesheet" href="/public/css/lavish.css">
		<link rel="stylesheet" href="/public/css/jquery-ui-1.8.18.custom.css">
    <link rel="stylesheet" href="/public/css/theme.css">
		
<?php if (isset($css)):
			  foreach($css as $val) : 
?>
			<link rel="stylesheet" href="/public/<?php echo $val;?>">
<?php   endforeach;
      endif; 
?>
    <script language="javascript" src="/public/js/jquery/jquery-1.7.1.min.js"></script>
    <script language="javascript" src="/public/js/jquery/jquery-ui-1.8.18.custom.min.js"></script>		
    <script language="javascript" src="/public/js/bootstrap/bootstrap.js"></script>		
<?php if (isset($js)):
			  foreach($js as $val) : 
?>
		 <script language="javascript" src="/public/<?php echo $val;?>"></script>
<?php   endforeach;
      endif; 
?>		
  </head>
  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">CONTENT DATA MANAGEMENT SYSTEM</a>
          <?php if($header) { ?>
            <div class="nav-collapse">              
              <ul class="nav pull-right">
							  <li class="divider-vertical"></li>
							  <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings<b class="caret"></b></a>
                  <ul class="dropdown-menu">               									
                    <li><a href="/logout">Logout</a></li>
                  </ul>
                </li>               
              </ul>							
            </div>
            <div style="float:right; margin:10px 0 0 0; padding:0; color:#fff;">
          	 <?php echo $this->session->userdata('username');  ?>
            </div>
          <?php } ?>
          <!-- /.nav-collapse -->
        </div>
      </div>
      <!-- /navbar-inner -->
    </div>
    <div class="bodyDiv container">
      <?php if($this->session->flashdata('msg')) { ?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('msg');?>
        </div>
      <?php } ?>
      <!--header view Ends-->
      <?php echo $template[ 'body']; ?>
    </div>
  </body>
</html>