<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Logout Controller
 *
 * @author Samier Sompura <samier.sompura@gmail.com>
*/
class Logout extends CI_Controller
{
  var $base_url = NULL;
  
  /*
   ** Load Libraries & model
  */
  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->base_url = base_url();
    $this->load->model('login_model');
  }
  
  /*
   ** Logout 
  */
  function index()
  {
    $this->session->sess_destroy();
    if (isset($_COOKIE['cookname']) && isset($_COOKIE['cookpass'])) {
      setcookie("cookname", "", time() - 60 * 60 * 24 * 100, "/");
      setcookie("cookpass", "", time() - 60 * 60 * 24 * 100, "/");
      setcookie("cookusertype", "", time() - 60 * 60 * 24 * 100, "/");
    }
    redirect('login');
  }
}
/* End of file logout.php */
/* Location: ./application/controllers/logout.php */