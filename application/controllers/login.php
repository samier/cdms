<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Login Controller
 *
 * @author Samier Sompura <samier.sompura@gmail.com>
 */
class Login extends CI_Controller
{  
  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');    
    $this->load->model('login_model');
	$this->load->library('template');
  }
  
  /**
   * Login Page
   */  
  public function index()
  {
    $data = array('header' => false);
	
	if ($this->session->userdata('id'))
		redirect('project');
		
    $this->template->set_layout('admin')->build('admin/login/login_view', $data);
  }
  
  /**
   * Check Login Process
   */ 
  public function submit()
  {
  	// Check Validation 
    $this->form_validation->set_rules("username", 'E-Mail', 'trim|required');
    $this->form_validation->set_rules("password", 'Password', 'trim|required');
	
    if ($this->form_validation->run() == FALSE) {
    	$this->form_validation->set_error_delimiters('<span class="error_message">', '</span>');
    	$data = array('header' => false);			
    	$this->template->set_layout('admin')->build('admin/login/login_view', $data);
    } else {
		$result = $this->login_model->check_login($this->input->post('username'), md5($this->input->post('password')));
		
		if ($result->num_rows() == 1) {
			$row = $result->row();
			$this->session->set_userdata('id', $row->id);
			$this->session->set_userdata('username', $row->username);
			$this->session->set_userdata('type', $row->type);
			redirect('project');
		} else {
			$this->session->set_flashdata('err_msg', 'Invalid E-mail or Password');
			redirect('login');
      }
    }
  }
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */