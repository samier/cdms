<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ProjectApi Controller
 * @author		Samier Sompura <samier.sompura@gmail.com>
 * @link		http://wamasoftware.com
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Projectapi extends REST_Controller
{
	var $CI;
	
	/*
	 ** Create CI Instance
	*/
	public function __construct()
  	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->CI->load->model('project_model');
	}
	
	/*
	 ** get Project data based on id
	*/
	function project_get()
    {
        if (!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }
	
		$projects = $this->CI->project_model->get_project_data_api($this->get('id'));
    	
    	$project = @$projects[$this->get('id')];
    	
        if ($project)
            $this->response($project, 200); // 200 being the HTTP response code
        else
            $this->response(array('error' => 'User could not be found'), 404);
    }
    
    function project_post()
    {
        $message = array('id' => $this->get('id'), 'name' => $this->post('name'), 'email' => $this->post('email'), 'message' => 'ADDED!');
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function project_delete()
    {
    	$message = array('id' => $this->get('id'), 'message' => 'DELETED!');
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
	/*
	 ** get All Project data based on id
	*/
    function projects_get()
    {
     	$projects = $this->CI->project_model->get_project_data_api();
		
        if ($projects)
           $this->response($projects, 200); // 200 being the HTTP response code
        else
           $this->response(array('error' => 'Couldn\'t find any users!'), 404);
    }

	public function send_post()
	{
		var_dump($this->request->body);
	}

	public function send_put()
	{
		var_dump($this->put('foo'));
	}
}

/* End of file projectapi.php */
/* Location: ./application/controllers/api/projectapi.php */