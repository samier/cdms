<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Project Controller
 *
 * @author Samier Sompura <samier.sompura@gmail.com>
*/
class project extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    if (!$this->session->userdata('id')) {
      redirect('login');
    }
    $this->load->model('project_model');
	$this->load->model('comment_model');
  }
	
	public function index()
	{
		$data['header'] = true;
		$data['js'] = array(
						"datatable/js/jquery.dataTables.min.js",
						"js/admin/project.js"
					  );
		$data['css'] = array(
						  "datatable/css/demo_table.css",
						  "datatable/css/demo_page.css"
					   );
		$data['user_type'] = $this->session->userdata('type');					   				 
		$this->template->set_layout('admin')->build('admin/project/project_list_view',$data);
	}
	
  function ajax_project_datatable()
  {
	  	header('content-type: application/json');
		exit($this->project_model->ajax_project_datatable_data($this->session->userdata('type')));
  }
  
  function ajax_project_datatable_new()
  {
	  	header('content-type: application/json');
		exit($this->project_model->ajax_project_datatable_data_new($this->session->userdata('type')));
  }
  
  /**
   * Add Action
   */  
  function add($id = NULL)
  {
  	if ($this->session->userdata('type') == 'c' and !isset($id))
	{
		$this->session->set_flashdata('msg', 'Resticted Access');
		redirect('project');
	}
		
    $data = array();
	$data['user_type'] = $this->session->userdata('type');
    $post = $this->input->post(NULL, TRUE);
        
    // loading form validation library.
    $this->load->library('form_validation');
    
    $this->form_validation->set_error_delimiters('<span class="error_message">', '</span>');
    
	$flag = ($data['user_type'] == 'a') ? $this->form_validation->run('project') : $this->form_validation->run('project_edit');
	
    //if ($this->form_validation->run('project') == TRUE) {
	 if ($flag) {	
      // check for unique record before storing to database
      $records = $this->project_model->check_unique_recored($post, $id);

      if ($records['record_count'] == 0) {
		  
        if (isset($id)) {
		  $comment = $post['comment'];
		  unset ($post['comment']);
		  $post['upldatedPerson'] = $this->session->userdata('id');
          $result = $this->project_model->update_record($post, $id);
		  $commentArr = array(
							'comment'	=>	$comment,
							'projectId'	=>	$id,
							'userId'	=>	$this->session->userdata('id')
						  );
						  
		  $result = $this->comment_model->add($commentArr);
		  $this->session->set_flashdata('msg', 'Project Updated Successfully.');          
        } else {
			
		  $comment = $post['comment'];
		  unset ($post['comment']);
		  if ($post['assignedPerson'] != '0')
		  	$post['assignedDate'] = date('Y-m-d H:i:s',time());
		  	
		  $post['createdPerson'] = $this->session->userdata('id');
          $result = $this->project_model->add($post);
		  
		  if ($result)
		  {
		  	$commentArr = array(
							'comment'	=>	$comment,
							'projectId'	=>	$result,
							'userId'	=>	$this->session->userdata('id')
						  );
						  
		  	$result = $this->comment_model->add($commentArr);
		  }
          $this->session->set_flashdata('msg', 'Project Created Successfully.');         
        }
		redirect('project');
      } else {
        $data['unique_project_name'] = 'Project Name Already Exits';
      }
    }
	
	// Get ALL Content Producers
	$this->load->model('user_model');
	$content_Producers = $this->user_model->get_content_Producers();
	$content_Producer_options = array();
	$content_Producer_options[0] = 'Select User';
	foreach ($content_Producers as $content_Producer) {
		$content_Producer_options[$content_Producer['id']] = $content_Producer['username'];
	}
	$data['content_Producer_options'] = $content_Producer_options;
    // Check Is Edit
    if (isset($id))
      $data['id'] = $id;
    
	// Check Is Post and Is Edit
    if (count($_POST) == 0 and isset($id)) {
      $data['edit_result'] = $this->project_model->get_record($id);
      $data['id']          = $id;
	  $data['comments']	   = $this->comment_model->get_records($id);
	}
	
    $data['header'] = true;

    $data['url'] = '/project/add';
    $data['focus'] = "project";
	
	$data['js'] = array(
					  "datatable/js/jquery.dataTables.min.js",
					  "js/admin/project_add.js"
					  );
	$data['css'] = array(
				      "datatable/css/demo_table.css",
					  "datatable/css/demo_page.css"
					);
	
    // Loads template Library
    $this->load->library('template');
    $this->template->set_layout('admin')->build('admin/project/project_add_view', $data);
  }
}

/* End of file project.php */
/* Location: ./application/controllers/project.php */