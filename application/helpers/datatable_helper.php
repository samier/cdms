<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Get User Name from Id
 * @id integer User table Id
 */
if ( ! function_exists('get_username'))
{
	function get_username($id)
	{
		$CI =& get_instance();
		$CI->load->model('user_model');
		$result = $CI->user_model->get_user($id);
		if (count($result))
			return $result['username'];
		else
			return '';	
	}
}

/**
 * Check Dealine achived or not
 * @deadlineDate date 
 */
if ( ! function_exists('is_deadline'))
{
	function is_deadline ($deadlineDate)
	{		
		if ($deadlineDate <= ( date('Y-m-d') . '00:00:00'))
			return '<span style="background-color:red">' . $deadlineDate . '</span>';
		else
		    return $deadlineDate;
	}
}


/**
 * Get Latest Comment from projectId
 * @id integer comment table Id
 */
if ( ! function_exists('get_comment'))
{
	function get_comment($projectId)
	{
		$CI =& get_instance();
		$CI->load->model('comment_model');
		$result = $CI->comment_model->get_latest_comment($projectId);
		if (count($result))
			return $result['comment'];
		else
			return '';	
	}
}

/**
 * replace Status with full name
 * @replaced_Status status 
 */
if ( ! function_exists('replaced_Status'))
{
	function replaced_Status ($status)
	{		
		if ($status == 'n')
			return 'New';
		else if ($status == 'o')
			return 'On-Progress';
		else if ($status == 'p')
			return 'Pending';
		else if ($status == 'c')
			return 'Closed';
		else
		    return '';
	}
}
/* End of file datatable_helper.php */
/* Location: ./system/helpers/datatable_helper.php */