<?php

$config = array('project' => array(
				 
							  array(
									 'field'   => 'projectName',
									 'label'   => 'Project Name',
									 'rules'   => 'trim|required'
								  ),

							  array(
									 'field'   => 'description',
									 'label'   => 'Description',
									 'rules'   => 'trim|required'
								  ),
							  array(
									 'field'   => 'deadlineDate',
									 'label'   => 'Deadline Date',
									 'rules'   => 'trim|required'
								  ),
							  array(
									 'field'   => 'comment',
									 'label'   => 'Comment',
									 'rules'   => 'trim|required'
								  ),
							   array(
									 'field'   => 'tags',
									 'label'   => 'Tags',
									 'rules'   => 'trim|required'
								  ),
							  array(
									 'field'   => 'keywords',
									 'label'   => 'Keywords',
									 'rules'   => 'trim|required'
								   )							  
							 ),
				'project_edit' => array(
							 
							  array(
									 'field'   => 'comment',
									 'label'   => 'Comment',
									 'rules'   => 'trim|required'
							  		)
							 ));
