<?php
class Comment_model extends CI_Model
{
	var $table;
	
  	function __construct()
  	{
    	// Call the Model constructor
    	parent::__construct();
    	$this->table = 'comment';
		
    }
  
  //get All Comment Records based on projectId
  function get_records($projectId)
  {
    return $this->db->select($this->table.'.*,tbl_login_info.username')
					->from($this->table)
					->join('tbl_login_info', $this->table.'.userId = tbl_login_info.id')
					->where($this->table.'.projectId', $projectId)
					->get()->result_array(); 
  }
 
  // Adding data
  function add($post)
  {
    $this->db->insert($this->table, $post);
    return mysql_insert_id();
  }
  
  /**	
   * Get Latest Comment by projectId
   * @id Integer comment Id
   */ 
  function get_latest_comment($projectId)
  {
    return $this->db->select('comment')
    		 		->from($this->table)
					->where('projectId',$projectId)
					->order_by('commentDate', 'desc')
					->get()
       			    ->row_array();
  }
}