<?php
class Project_model extends CI_Model
{
	var $CI;
	
  	var $table;
	
  	function __construct()
  	{
    	// Call the Model constructor
    	parent::__construct();
    	$this->table = 'project';
		
    }
	
	public function ajax_project_datatable_data ($type) 
	{	
		$this->load->helper('datatable');	
		$this->load->library('datatables');		
		
		$this->datatables->select("id,projectName,description,status,tags,keywords,createdPerson,upldatedPerson,
			assignedPerson,assignedDate,deadlineDate,editedDate,dateCreated",false);
		$this->datatables->from($this->table);
		
		if ($type == 'c')
			$this->datatables->where('createdPerson ='.$this->session->userdata('id').' or upldatedPerson ='
									.$this->session->userdata('id').' or assignedPerson ='
									.$this->session->userdata('id'));
		
		$this->datatables->edit_column('deadlineDate','$1','is_deadline(deadlineDate)');
		$this->datatables->edit_column('createdPerson','$1','get_username(createdPerson)');
		$this->datatables->edit_column('upldatedPerson','$1','get_username(upldatedPerson)');
		$this->datatables->edit_column('assignedPerson','$1','get_username(assignedPerson)');
		$this->datatables->edit_column('status','$1','replaced_Status(status)');
		$this->datatables->add_column('comment', '$1', 'get_comment(id)');
						
    	return $this->datatables->generate();
	}
	
	public function ajax_project_datatable_data_new ($type) 
	{	
		$this->load->helper('datatable');	
		$this->load->library('datatables');		
		
		$this->datatables->select("id,status,tags,keywords,createdPerson,upldatedPerson,
			assignedPerson,assignedDate,deadlineDate,editedDate,dateCreated",false);
		$this->datatables->from($this->table);
		
		if ($type == 'c')
			$this->datatables->where('createdPerson ='.$this->session->userdata('id').' or upldatedPerson ='
									.$this->session->userdata('id').' or assignedPerson ='
									.$this->session->userdata('id'));
		
		$this->datatables->edit_column('deadlineDate','$1','is_deadline(deadlineDate)');
		$this->datatables->edit_column('createdPerson','$1','get_username(createdPerson)');
		$this->datatables->edit_column('upldatedPerson','$1','get_username(upldatedPerson)');
		$this->datatables->edit_column('assignedPerson','$1','get_username(assignedPerson)');
		$this->datatables->edit_column('status','$1','replaced_Status(status)');
		$this->datatables->add_column('comment', '$1', 'get_comment(id)');
						
    	return $this->datatables->generate();
	}
	
  // update Leads Data
  function update_record($post, $id)
  {
    $this->db->where('id', $id);
    $record = $this->db->update($this->table, $post);
    return $record;
  }
  
  //get single row for editing
  function get_record($id)
  {
    $rec = $this->db->where_in('id', $id)->limit(1)->get($this->table)->row_array();
    return $rec;
  }
 
  // Adding data
  function add($post)
  {
	$this->db->set('dateCreated','now()', FALSE); 
    $this->db->insert($this->table, $post);
	
    return mysql_insert_id();
  }
  
  // check for duplicate records
  function check_unique_recored($post, $id)
  {
    if (!isset($id))
         $id = 0;
		 
    return $this->db->select('count(id) as record_count')
					->where('projectName', $post['projectName'])
					->where('id !=', $id)
					->limit(1)
					->get($this->table)
					->row_array();
    
  }
  
  /*
   ** get All project data for Api
  */ 
  function get_project_data_api()
  {
	   return $this->db->select('*')
					->get($this->table)
					->result_array();
  }
}