<?php
class User_model extends CI_Model
{
  function __construct()
  {
    // Call the Model constructor
    parent::__construct();
  }
  
  /**
   * Get All Content Producers 
   */
  function get_content_Producers ()
  {
    return $this->db->select('id,username')
    		 		->where('type','c')
    		 		->from('tbl_login_info')
					->get()
       			    ->result_array();
  }

  /**	
   * Get User by Id
   * @id Integer User Id
   */ 
  function get_user ($id)
  {
    return $this->db->select('username')
    		 		->from('tbl_login_info')
					->where('id',$id)
					->get()
       			    ->row_array();
  }
    
}