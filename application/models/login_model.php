<?php
class Login_model extends CI_Model
{
  function __construct()
  {
    // Call the Model constructor
    parent::__construct();
  }
  
  /**
   * Check Valid User
   * @username username of the user
   * @password user password 
   * @return User Object or NULL if not found
   */ 
  function check_login($username, $password)
  {
    return $this->db->select('id,username,type')
    				->where('username', $username)
    				->where('password', $password)
    				->limit(1)
				    ->from('tbl_login_info')
    				->get();
  }
}