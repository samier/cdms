$(document).ready(function() {

 var gaiSelected =  [];

 var oTable = $('#project_table').dataTable( {
    "bJQueryUI": true,
    "bSortClasses": false,
    "bAutoWidth": true,
    "bInfo": true,
    "sScrollY": "100%",
    "sScrollX": "100%",
    "bScrollCollapse": true,
    "sPaginationType": "full_numbers",
    "bRetrieve": true,
    "oLanguage": {
        "sSearch": "Search Anything:"
    },
    "bProcessing": true,
    "bServerSide": true,
		"sAjaxSource": "/project/ajax_project_datatable/",
		"sServerMethod": "POST",
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {			
			$(nRow).css('cursor','pointer');
			return nRow;
		}
   });

	 /* Click event handler */
	$('#project_table tbody tr').live('click', function () {																												
		var aData = oTable.fnGetData( this );
		var iId = aData[0];		
		window.location.href = '/project/add/' + iId;
	} );	
	
});